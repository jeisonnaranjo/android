package com.janh.dataobject;


import java.io.Serializable;

public class Vehiculo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public String id;
	public String placa;
	public String fecha;
	public String hora;
	public String fechaConsulta;
	public boolean contravencion;
	public int cantidadContravencion;
	public String dia;
	public String timeStamp;
}
