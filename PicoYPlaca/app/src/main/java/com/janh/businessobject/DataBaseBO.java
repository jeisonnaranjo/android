package com.janh.businessobject;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.janh.dataobject.PicoPlaca;
import com.janh.dataobject.Vehiculo;
import com.janh.picoyplaca.Constantes;
import com.janh.picoyplaca.Utilities;

import java.io.File;
import java.util.Vector;

public class DataBaseBO {
  
  public static final String TAG = DataBaseBO.class.getName();
  
  public static void cerrarDataBase(SQLiteDatabase db) {
    if (db != null) {
      if (db.inTransaction()) {
        db.endTransaction();
      }
      db.close();
    }
  }
  
  public static boolean crearDataBase() {
    SQLiteDatabase db = null;
    try {
      File dbFile = new File(Utilities.dirApp(), "DBPicoPlaca.db");
      db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
      
      /*Se crea la tabla para almacenar la bitacora*/
      String tablaBitacora = "CREATE TABLE IF NOT EXISTS Bitacora(Id varchar(20), Placa varchar(20), Fecha datetime, Hora time, " +
                               "Dia varchar(20), Contravencion bit, TimeStamp datetime) ";
      db.execSQL(tablaBitacora);
      
      /*Se crea la tabla para almacenar la programacion del pico y placa*/
      String tablaPicoPlaca = "CREATE TABLE IF NOT EXISTS PicoPlaca(Dia varchar(20), Numeros varchar(20), HoraMananaInicio time,  HoraMananaFin time, " + "HoraTardeInicio time,  HoraTardeFin time, TimeStamp datetime); ";
      db.execSQL(tablaPicoPlaca);
      
      /*Se elimina la programacion del pico y placa*/
      String borrarDatosTablaPicoPlaca = "DELETE FROM PicoPlaca ";
      db.execSQL(borrarDatosTablaPicoPlaca);
      
      
      /*Se inserta la programacion del pico y placa*/
      String timeStamp = Utilities.fechaActual(Constantes.FORMATO_FECHA);
      String insertarDatosTablaPicoPlaca = "INSERT INTO PicoPlaca(Dia, Numeros, HoraMananaInicio, HoraMananaFin, HoraTardeInicio, HoraTardeFin, TimeStamp) " +
                                             "VALUES ('LUNES', '1,2', '07:00', '09:30', '16:00', '19:30', '" + timeStamp + "') ";
      db.execSQL(insertarDatosTablaPicoPlaca);
      insertarDatosTablaPicoPlaca = "INSERT INTO PicoPlaca(Dia, Numeros, HoraMananaInicio, HoraMananaFin, HoraTardeInicio, HoraTardeFin, TimeStamp) " +
                                      "VALUES ('MARTES', '3,4', '07:00', '09:30', '16:00', '19:30', '" + timeStamp + "') ";
      db.execSQL(insertarDatosTablaPicoPlaca);
      insertarDatosTablaPicoPlaca = "INSERT INTO PicoPlaca(Dia, Numeros, HoraMananaInicio, HoraMananaFin, HoraTardeInicio, HoraTardeFin, TimeStamp) " +
                                      "VALUES ('MIERCOLES', '5,6', '07:00', '09:30', '16:00', '19:30', '" + timeStamp + "') ";
      db.execSQL(insertarDatosTablaPicoPlaca);
      insertarDatosTablaPicoPlaca = "INSERT INTO PicoPlaca(Dia, Numeros, HoraMananaInicio, HoraMananaFin, HoraTardeInicio, HoraTardeFin, TimeStamp) " +
                                      "VALUES ('JUEVES', '7,8', '07:00', '09:30', '16:00', '19:30', '" + timeStamp + "') ";
      db.execSQL(insertarDatosTablaPicoPlaca);
      insertarDatosTablaPicoPlaca = "INSERT INTO PicoPlaca(Dia, Numeros, HoraMananaInicio, HoraMananaFin, HoraTardeInicio, HoraTardeFin, TimeStamp) " +
                                      "VALUES ('VIERNES', '9,0', '07:00', '09:30', '16:00', '19:30', '" + timeStamp + "') ";
      db.execSQL(insertarDatosTablaPicoPlaca);
      
      
      return true;
    } catch (Exception e) {
      Log.e(TAG, "Error creando la base de datos -> " + e.getMessage());
      return false;
    } finally {
      if (db != null) {
        cerrarDataBase(db);
      }
    }
  }
  
  
  
  public static boolean validarPicoPlaca(Vehiculo vehiculo) {
    boolean estado = false;
    SQLiteDatabase db = null;
    try {
      File dbFile = new File(Utilities.dirApp(), "DBPicoPlaca.db");
      if (dbFile.exists()) {
        db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
        String ultimoDigitoPlaca = vehiculo.placa.charAt(vehiculo.placa.length()-1)+"";
        String hora = vehiculo.hora.replace(":","");
        String query = "SELECT COUNT(*) AS Total " +
                         "FROM PicoPlaca WHERE Dia = '" + vehiculo.dia + "' " +
                         "AND Numeros LIKE '%"+ultimoDigitoPlaca+"%' " +
                         "AND " +
                         "(strftime('%H%M', HoraMananaInicio) <= '" + hora + "' AND strftime('%H%M', HoraMananaFin) >= '" + hora + "' " +
                         "OR " +
                         "strftime('%H%M', HoraTardeInicio) <= '" + hora + "' AND strftime('%H%M', HoraTardeFin) >= '" + hora + "') ";
        
        Log.i(TAG,"validarPicoPlaca -> " + query);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
          if (cursor.moveToFirst()) {
            int total = cursor.getInt(cursor.getColumnIndex("Total"));
            estado = total>0;
          }
        }
      }
    } catch (Exception e) {
      Log.e(TAG, "validarPicoPlaca -> ERROR ", e);
      return false;
    } finally {
      if (db != null) {
        cerrarDataBase(db);
      }
    }
    return estado;
  }
  
  
  
  
  public static PicoPlaca obtenerPicoPlaca(Vehiculo vehiculo) {
    PicoPlaca picoPlaca = null;
    SQLiteDatabase db = null;
    try {
      File dbFile = new File(Utilities.dirApp(), "DBPicoPlaca.db");
      if (dbFile.exists()) {
        db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
        String ultimoDigitoPlaca = vehiculo.placa.charAt(vehiculo.placa.length()-1)+"";
        String query = "SELECT Dia, Numeros, HoraMananaInicio, HoraMananaFin, HoraTardeInicio, HoraTardeFin, " +
                         "(SELECT CASE WHEN SUM(Contravencion) IS NULL THEN '0' ELSE SUM(Contravencion) END AS CantidadContravencion  " +
                         " FROM Bitacora WHERE Placa = '"+vehiculo.placa+"') AS CantidadContravencion " +
                         "FROM PicoPlaca " +
                         "WHERE Numeros LIKE '%"+ultimoDigitoPlaca+"%' ";
        
        Log.i(TAG,"obtenerPicoPlaca -> " + query);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
          if (cursor.moveToFirst()) {
            do {
              picoPlaca = new PicoPlaca();
              picoPlaca.dia = cursor.getString(cursor.getColumnIndex("Dia"));
              picoPlaca.descripcionDia = cursor.getString(cursor.getColumnIndex("Dia"));
              picoPlaca.numero = cursor.getString(cursor.getColumnIndex("Numeros"));
              picoPlaca.horarioM = "De "+cursor.getString(cursor.getColumnIndex("HoraMananaInicio")) + " Hasta " + cursor.getString(cursor.getColumnIndex("HoraMananaFin"));
              picoPlaca.horarioT = "De "+cursor.getString(cursor.getColumnIndex("HoraTardeInicio")) + " Hasta " + cursor.getString(cursor.getColumnIndex("HoraTardeFin"));
              picoPlaca.cantidadContravencionXPlaca = cursor.getString(cursor.getColumnIndex("CantidadContravencion"));
            }
            while (cursor.moveToNext());
    
          }
        }
      }
    } catch (Exception e) {
      Log.e(TAG, "obtenerPicoPlaca -> ERROR ", e);
    } finally {
      if (db != null) {
        cerrarDataBase(db);
      }
    }
    return picoPlaca;
  }
  
  
  
  public static boolean guardarBitacora(Vehiculo vehiculo) {
    SQLiteDatabase db = null;
    try {
      String fechaActual = Utilities.fechaActual(Constantes.FORMATO_FECHA);
      File dbFile = new File(Utilities.dirApp(), "DBPicoPlaca.db");
      if (dbFile.exists()) {
        db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
        ContentValues values = new ContentValues();
        values.put("Id", vehiculo.id);
        values.put("Placa", vehiculo.placa);
        values.put("Fecha", vehiculo.fecha);
        values.put("Hora", vehiculo.hora);
        values.put("Dia", vehiculo.dia);
        values.put("Contravencion", vehiculo.contravencion);
        values.put("TimeStamp", fechaActual);
        double insertDb = db.insertOrThrow("Bitacora", null, values);
      }
      return true;
    } catch (Exception e) {
      Log.e(TAG, "guardarBitacora -> ERROR ", e);
      return false;
    } finally {
      if (db != null) {
        cerrarDataBase(db);
      }
    }
  }
  
  
  
  
  public static Vector<Vehiculo> listarBitacora() {
    
    SQLiteDatabase db = null;
    Vector<Vehiculo> listaBitacora = new Vector<Vehiculo>();
    Vehiculo vehiculo;
    try {
      File dbFile = new File(Utilities.dirApp(), "DBPicoPlaca.db");
      if (dbFile.exists()) {
        db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
        
        String query = "SELECT Id, Placa, Fecha, Hora, Dia, Contravencion, TimeStamp, '0' AS CantidadContravencion " +
                         "FROM Bitacora ";
  
        /*Consulta agrupando por placa y consultando la cantidad de contravenciones*/
        /*String query = "SELECT Id, Placa, Fecha, Hora, Dia, Contravencion, TimeStamp, SUM(Contravencion) AS CantidadContravencion " +
                         "FROM Bitacora GROUP BY Placa ";*/
        
        Log.i( TAG, "listarBitacora->" + query );
        
        Cursor cursor = db.rawQuery( query, null );
        if ( cursor.moveToFirst() ) {
          do {
            vehiculo = new Vehiculo();
            vehiculo.id = cursor.getString( cursor.getColumnIndex( "Id" ) );
            vehiculo.placa = cursor.getString( cursor.getColumnIndex( "Placa" ) );
            vehiculo.fecha = cursor.getString( cursor.getColumnIndex( "Fecha" ) );
            vehiculo.hora = cursor.getString( cursor.getColumnIndex( "Hora" ) );
            vehiculo.dia = cursor.getString( cursor.getColumnIndex( "Dia" ) );
            vehiculo.contravencion = cursor.getInt( cursor.getColumnIndex( "Contravencion" ) ) == 1 ? true:false;
            vehiculo.timeStamp = cursor.getString( cursor.getColumnIndex( "TimeStamp" ) );
            vehiculo.cantidadContravencion = cursor.getInt( cursor.getColumnIndex( "CantidadContravencion" ) );
            listaBitacora.addElement( vehiculo );
          } while (cursor.moveToNext());
        }
      }
    }  catch (Exception e) {
      Log.e(TAG, "listarBitacora -> ERROR ", e);
    } finally {
      if (db != null) {
        cerrarDataBase(db);
      }
    }
    return listaBitacora;
  }
  
  
}


