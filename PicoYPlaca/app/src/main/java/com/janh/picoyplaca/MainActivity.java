package com.janh.picoyplaca;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.janh.businessobject.DataBaseBO;
import com.janh.dataobject.PicoPlaca;
import com.janh.dataobject.Vehiculo;

import java.io.File;
import java.util.Calendar;

import androidx.appcompat.app.ActionBar;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class MainActivity extends CustomActivity implements View.OnClickListener {
  
  public static final String TAG = MainActivity.class.getName();
  
  private EditText edtPlaca;
  private EditText edtFecha;
  private ImageButton btnFecha;
  private EditText edtHora;
  private ImageButton btnHora;
  private Button btnConsultar;
  private FabSpeedDial fabSpeedDial;
  
  DatePickerDialog datePickerDialog;
  TimePickerDialog timePickerDialog;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    //DEFINE BOTON ATRAS LISTENER onOptionsItemSelected()
    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    init();
  }
  
  private void init() {
    edtPlaca = (EditText) findViewById(R.id.edtPlaca);
    edtFecha = (EditText) findViewById(R.id.edtFecha);
    btnFecha = (ImageButton) findViewById(R.id.btnFecha);
    edtHora = (EditText) findViewById(R.id.edtHora);
    btnHora = (ImageButton) findViewById(R.id.btnHora);
    btnConsultar = (Button) findViewById(R.id.btnConsultar);
    fabSpeedDial = (FabSpeedDial) findViewById(R.id.fabSpeedDial);
    edtPlaca.setFilters(new InputFilter[]{Utilities.filterMayusculas});
    btnFecha.setOnClickListener(this);
    btnHora.setOnClickListener(this);
    btnConsultar.setOnClickListener(this);
    fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
      @Override
      public boolean onMenuItemSelected(MenuItem menuItem) {
        Log.e(TAG, "menuItem " + menuItem.toString() + " id " + menuItem.getItemId());
        switch (menuItem.getItemId()) {
          case R.id.action_bitacora:
            abrirBitacora();
            return true;
        }
        return false;
      }
    });
  }
  
  public void abrirBitacora() {
    Intent formClienteNuevo = new Intent(this, BitacoraActivity.class);
    startActivity(formClienteNuevo);
  }
  
  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btnFecha:
        Log.i(TAG, "onClick -> edtFecha");
        clickFecha(view);
        break;
      case R.id.btnHora:
        Log.i(TAG, "onClick -> edtHora");
        clickHora(view);
        break;
      case R.id.btnConsultar:
        Log.i(TAG, "onClick -> btnConsultar");
        consultarPicoYPlaca();
        break;
      default:
        break;
    }
    
  }
  
  /*Metodo para seleccionar la fecha*/
  public void clickFecha(final View view) {
    DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
      @Override
      public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        EditText edtFechaOut = edtFecha;
        String day = "";
        String month = "";
        ++monthOfYear;
        if (dayOfMonth < 10) {
          day = "0" + dayOfMonth;
        } else {
          day = "" + dayOfMonth;
        }
        if (monthOfYear < 10) {
          month = "0" + monthOfYear;
        } else {
          month = "" + monthOfYear;
        }
        if (edtFechaOut != null) {
          String fechaSeleccionada = year + "-" + month + "-" + day;
          edtFechaOut.setText(fechaSeleccionada);
        }
      }
    };
    Calendar cal = Calendar.getInstance();
    datePickerDialog = new DatePickerDialog(this, listener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    datePickerDialog.show();
  }
  
  /*Metodo para seleccionar la hora*/
  public void clickHora(final View view) {
    TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
      @Override
      public void onTimeSet(TimePicker timePicker, int horaIn, int minutoIn) {
        EditText edtFechaOut = edtHora;
        String hora = "";
        String minuto = "";
        if (horaIn < 10) {
          hora = "0" + horaIn;
        } else {
          hora = "" + horaIn;
        }
        if (minutoIn < 10) {
          minuto = "0" + minutoIn;
        } else {
          minuto = "" + minutoIn;
        }
        if (edtFechaOut != null) {
          String horaSeleccionada = hora + ":" + minuto;
          edtFechaOut.setText(horaSeleccionada);
        }
      }
    };
    Calendar cal = Calendar.getInstance();
    timePickerDialog = new TimePickerDialog(this, listener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false);
    timePickerDialog.show();
  }
  
  public void consultarPicoYPlaca() {
    File dbFile = new File(Utilities.dirApp(), "DBPicoPlaca.db");
    if (!dbFile.exists()) {
      DataBaseBO.crearDataBase();
    }
    String placa = edtPlaca.getText().toString().trim();
    String fecha = edtFecha.getText().toString().trim();
    String hora = edtHora.getText().toString().trim();
    if (placa.equals("")) {
      Utilities.mostrarAlerta(this, "Por favor ingrese la placa del vehiculo");
      edtPlaca.requestFocus();
      return;
    }
    if (fecha.equals("")) {
      Utilities.mostrarAlerta(this, "Por favor ingrese la fecha");
      edtFecha.requestFocus();
      return;
    }
    if (hora.equals("")) {
      Utilities.mostrarAlerta(this, "Por favor ingrese la hora");
      edtHora.requestFocus();
      return;
    }
    Log.i(TAG, "consultarPicoYPlaca -> fecha " + fecha);
    int dia = Utilities.getDiaSemana(fecha);
    Log.i(TAG, "consultarPicoYPlaca -> dia " + dia);
    String descripcionDia = Utilities.obtenerDescripcionDia(dia + "");
    Log.i(TAG, "consultarPicoYPlaca -> descripcionDia " + descripcionDia);
    Vehiculo vehiculo = new Vehiculo();
    vehiculo.id = Utilities.fechaActual(Constantes.FORMATO_FECHA_ID);
    vehiculo.placa = placa;
    vehiculo.fecha = fecha;
    vehiculo.hora = hora;
    vehiculo.dia = descripcionDia;
    boolean tienePicoPlaca = DataBaseBO.validarPicoPlaca(vehiculo);
    vehiculo.contravencion = tienePicoPlaca;
    //se guarda el registro de bitacora
    DataBaseBO.guardarBitacora(vehiculo);
    PicoPlaca picoPlaca = DataBaseBO.obtenerPicoPlaca(vehiculo);
    String mensaje = "";
    if (picoPlaca != null) {
      mensaje = "\n\nSu pico y placa es el dia " + picoPlaca.dia + " en los horarios " + picoPlaca.horarioM + " " + "y " + picoPlaca.horarioT + "\n\nContravencion de su vehiculo " + picoPlaca.cantidadContravencionXPlaca;
    }
    if (tienePicoPlaca) {
      Utilities.mostrarAlerta(this, "Su vehiculo SI tiene pico y placa en este horario.\n\n" + "Por favor no circule con su vehiculo, de lo contrario podria ser sancionado." + mensaje);
    } else {
      Utilities.mostrarAlerta(this, "Su vehiculo NO tiene pico y placa en este horario." + mensaje);
    }
    edtPlaca.setText("");
    edtFecha.setText("");
    edtHora.setText("");
    
  }
  
  public void mensajeSalir() {
    final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.dialog_generico_opcion);
    ((TextView) dialog.findViewById(R.id.txtMensaje)).setText("¿Desea salir de la aplicación?");
    ((Button) dialog.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.cancel();
        salir();
      }
    });
    ((Button) dialog.findViewById(R.id.btnCancelar)).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.cancel();
      }
    });
    dialog.setCancelable(false);
    dialog.show();
  }
  
  /*METODO PRESIONAR BOTON ATRAS (HARDWARE)-FISICO API<17*/
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      mensajeSalir();
    }
    return true;
  }
  
  /*METODO PRESIONAR BOTON ATRAS (HARDWARE)-FISICO*/
  @Override
  public void onBackPressed() {
    mensajeSalir();
  }
  
  /*METODO PRESIONAR BOTON ATRAS (SOFTWARE)-TOOLBAR*/
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        mensajeSalir();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
  
  private void salir() {
    finish();
  }
  
}
