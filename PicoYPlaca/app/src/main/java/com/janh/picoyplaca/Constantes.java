package com.janh.picoyplaca;
public class Constantes {
  
  public final static String NOMBRE_DIRECTORIO = "PicoYPlaca";
  
  public final static String FORMATO_FECHA = "yyyy-MM-dd HH:mm:ss";
  public final static String FORMATO_FECHA_CORTA = "yyyy-MM-dd";
  public final static String FORMATO_FECHA_CORTA_ID = "yyyyMMdd";
  public final static String FORMATO_FECHA_ID = "yyyyMMddHHmmssSSS";
  public final static String FORMATO_TIEMPO = "HH:mm:ss";
  
  public final static int PERMISOS_STORAGE = 1000;
  
  public final static int LUNES = 1; // Calendar.MONDAY (2)
  public final static int MARTES = 2; // Calendar.TUESDAY (3)
  public final static int MIERCOLES = 3; // Calendar.WEDNESDAY (4)
  public final static int JUEVES = 4; // Calendar.THURSDAY (5)
  public final static int VIERNES = 5; // Calendar.FRIDAY (6)
  public final static int SABADO = 6; // Calendar.SATURDAY (7)
  public final static int DOMINGO = 7; // Calendar.SUNDAY (1)
  
  public final static String LUNES_ST = "LUNES";
  public final static String MARTES_ST = "MARTES";
  public final static String MIERCOLES_ST = "MIERCOLES";
  public final static String JUEVES_ST = "JUEVES";
  public final static String VIERNES_ST = "VIERNES";
  public final static String SABADO_ST = "SABADO";
  public final static String DOMINGO_ST = "DOMINGO";
  
}
