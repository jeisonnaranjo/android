package com.janh.picoyplaca;

import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.janh.adapters.RecyclerViewAdapterBitacora;
import com.janh.businessobject.DataBaseBO;
import com.janh.dataobject.Vehiculo;

import java.util.Vector;

import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BitacoraActivity extends CustomActivity {
  
  public static final String TAG = BitacoraActivity.class.getName();
  
  TextView txtVacio;
  RecyclerView rcwBitacora;
  RecyclerViewAdapterBitacora adapterBitacora;
  
  Vector<Vehiculo> listaBitacora;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_bitacora);
    //DEFINE BOTON ATRAS LISTENER onOptionsItemSelected()
    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    init();
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    cargarBitacora();
  }
  
  private void init() {
    txtVacio = (TextView) findViewById(R.id.txtVacio);
    rcwBitacora = (RecyclerView) findViewById(R.id.rcwBitacora);
    //rcwBitacora.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
    rcwBitacora.setLayoutManager(new LinearLayoutManager(this));
    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rcwBitacora.getContext(), DividerItemDecoration.VERTICAL);
    rcwBitacora.addItemDecoration(dividerItemDecoration);
  }
  
  public void cargarBitacora() {
    listaBitacora = DataBaseBO.listarBitacora();
    if (listaBitacora.size() == 0) {
      txtVacio.setVisibility(View.VISIBLE);
    } else {
      txtVacio.setVisibility(View.GONE);
    }
    if (rcwBitacora == null) {
      rcwBitacora = (RecyclerView) findViewById(R.id.rcwBitacora);
      //rcwBitacora.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
      rcwBitacora.setLayoutManager(new LinearLayoutManager(this));
      DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rcwBitacora.getContext(), DividerItemDecoration.VERTICAL);
      rcwBitacora.addItemDecoration(dividerItemDecoration);
    }
    rcwBitacora.setItemViewCacheSize(listaBitacora.size());
    adapterBitacora = new RecyclerViewAdapterBitacora(this, listaBitacora);
    rcwBitacora.setAdapter(adapterBitacora);
  }
  
  
  
  
  /*METODO PRESIONAR BOTON ATRAS (HARDWARE)-FISICO API<17*/
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      salir();
    }
    return true;
  }
  
  /*METODO PRESIONAR BOTON ATRAS (HARDWARE)-FISICO*/
  @Override
  public void onBackPressed() {
    salir();
  }
  
  /*METODO PRESIONAR BOTON ATRAS (SOFTWARE)-TOOLBAR*/
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        salir();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
  
  private void salir() {
    finish();
  }
  
}
