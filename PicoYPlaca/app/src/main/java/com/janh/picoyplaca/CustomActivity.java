package com.janh.picoyplaca;
import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.janh.businessobject.DataBaseBO;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class CustomActivity extends AppCompatActivity {
  public static final String TAG = CustomActivity.class.getName();
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //Allowing Strict mode policy for Nougat support
    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
    StrictMode.setVmPolicy(builder.build());
    checkPermission();
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    checkPermission();
    if (getCurrentFocus() != null) {
      InputMethodManager inputMethodManager = (InputMethodManager) getApplication().getSystemService(this.INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
  }
  
  /********************************************************************************************************************************
   * A continuacion se definen los metodos para validar permisos.
   * La idea es verificar siempre que se reanuda la aplicacion si tiene los permisos activos.
   ********************************************************************************************************************************/
  private void checkPermission() {
    //permisos almacenamiento
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
          && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
      checkPermissionStorage();
    }
  }
  
  private void checkPermissionStorage() {
    Log.e(TAG, "checkPermissionStorage-> ");
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
          && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
      if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constantes.PERMISOS_STORAGE);
      } else {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constantes.PERMISOS_STORAGE);
      }
    }
  }
  
  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    switch (requestCode) {
      case Constantes.PERMISOS_STORAGE: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission granted
          Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
          checkPermission();
          DataBaseBO.crearDataBase();
        } else {
          // permission denied
          Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
          checkPermissionStorage();
        }
        return;
      }
    }
  }
  
}
