package com.janh.picoyplaca;
import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class Utilities {
  
  public final static String TAG = Utilities.class.getName();
  
  public static String fechaActual(String format) {
    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    return sdf.format(date);
  }
  
  public static File dirApp() {
    File SDCardRoot = Environment.getExternalStorageDirectory();
    File dirApp = new File(SDCardRoot.getPath() + "/" + Constantes.NOMBRE_DIRECTORIO);
    if (!dirApp.isDirectory()) {
      dirApp.mkdirs();
    }
    return dirApp;
  }
  
  public static int getDiaSemana(String fecha) {
    int dia = 0;
    Date date = convertirFecha(fecha, Constantes.FORMATO_FECHA_CORTA);
    Calendar calendario = Calendar.getInstance();
    calendario.setFirstDayOfWeek(Calendar.MONDAY);
    calendario.setTime(date);
    dia = calendario.get(Calendar.DAY_OF_WEEK) - 1;
    Log.i(TAG, "getDiaSemana -> " + dia);
    return dia;
  }
  
  public static Date convertirFecha(String date, String format) {
    if (format.equals("")) {
      format = "yyyy-MM-dd";
    }
    SimpleDateFormat fecha = new SimpleDateFormat(format);
    Date valorFecha = null;
    try {
      valorFecha = fecha.parse(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return valorFecha;
  }
  
  public static String obtenerDescripcionDia(String codigoDia) {
    String descripcionDia = "";
    if (codigoDia.equals(Constantes.LUNES + "")) {
      descripcionDia = Constantes.LUNES_ST;
    }
    if (codigoDia.equals(Constantes.MARTES + "")) {
      descripcionDia = Constantes.MARTES_ST;
    }
    if (codigoDia.equals(Constantes.MIERCOLES + "")) {
      descripcionDia = Constantes.MIERCOLES_ST;
    }
    if (codigoDia.equals(Constantes.JUEVES + "")) {
      descripcionDia = Constantes.JUEVES_ST;
    }
    if (codigoDia.equals(Constantes.VIERNES + "")) {
      descripcionDia = Constantes.VIERNES_ST;
    }
    if (codigoDia.equals(Constantes.SABADO + "")) {
      descripcionDia = Constantes.SABADO_ST;
    }
    if (codigoDia.equals(Constantes.DOMINGO + "")) {
      descripcionDia = Constantes.DOMINGO_ST;
    }
    return descripcionDia;
  }
  
  public static void mostrarAlerta(Context context, String mensaje) {
    final Dialog dialog = new Dialog(context);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.dialog_generico);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    ((TextView) dialog.findViewById(R.id.txtMensaje)).setText(mensaje);
    ((Button) dialog.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.cancel();
      }
    });
    dialog.setCancelable(false);
    dialog.show();
  }
  
  public static final InputFilter filterMayusculas = new InputFilter.AllCaps();
  
}
