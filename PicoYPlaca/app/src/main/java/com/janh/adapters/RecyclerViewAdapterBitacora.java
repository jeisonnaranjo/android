package com.janh.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.janh.dataobject.Vehiculo;
import com.janh.picoyplaca.R;

import java.util.Vector;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapterBitacora extends RecyclerView.Adapter<RecyclerViewAdapterBitacora.AdapterViewHolder> {
  
  public static final String TAG = RecyclerViewAdapterBitacora.class.getName();
  
  Context context;
  Vector<Vehiculo> listaBitacora;
  
  public RecyclerViewAdapterBitacora(Context context, Vector<Vehiculo> listaVehiculosCambios) {
    this.listaBitacora = listaVehiculosCambios;
    this.context = context;
  }
  
  public AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_bitacora, parent, false);
    return new AdapterViewHolder(view);
  }
  
  public void onBindViewHolder(RecyclerViewAdapterBitacora.AdapterViewHolder holder, int position) {
    Vehiculo vehiculo = listaBitacora.get(position);
    if (vehiculo != null) {
      String titulo = vehiculo.placa;
      String subtitulo = "Dia Consulta: " + vehiculo.dia + "\n" +
                         "Fecha: " + vehiculo.fecha + "   Hora: " + vehiculo.hora + "\n" +
                         "Contravencion: " + (vehiculo.contravencion ? "Si" : "No") + "\n" +
                         //"Cantidad Contravencion: " + vehiculo.cantidadContravencion + "\n" +
                         "Fecha Consulta: " + vehiculo.timeStamp;
      holder.lblTitulo.setText(titulo);
      holder.lblSubTitulo.setText(subtitulo);
    }
    
  }
  
  public class AdapterViewHolder extends RecyclerView.ViewHolder {
    
    LinearLayout lytListItem;
    CardView itmCardView;
    TextView lblTitulo;
    TextView lblSubTitulo;
    
    public AdapterViewHolder(View itemView) {
      super(itemView);
      lytListItem = (LinearLayout) itemView.findViewById(R.id.lytListItem);
      itmCardView = (CardView) itemView.findViewById(R.id.itmCardView);
      lblTitulo = (TextView) itemView.findViewById(R.id.lblTitulo);
      lblSubTitulo = (TextView) itemView.findViewById(R.id.lblSubTitulo);
    }
  }
  
  public int getItemCount() {
    return listaBitacora.size();
  }
  
  public Vehiculo getItem(int position) {
    Vehiculo Vehiculo = listaBitacora.get(position);
    return Vehiculo;
  }
  
  public void setData(Vector<Vehiculo> listaVehiculos) {
    this.listaBitacora = listaVehiculos;
  }
  
  public Vector<Vehiculo> getData() {
    return listaBitacora;
  }
  
}
